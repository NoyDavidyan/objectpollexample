package com.example.gmexercise;

/**
 * Created by Noy davidyan on 26/11/2021.
 */

public interface CallBack {
    void returnItemToReuse(SomeObject someObject);
}
