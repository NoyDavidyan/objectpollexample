package com.example.gmexercise;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    // the sample default values
    final private int TEST_NUMBER = 1;
    final private String TEST_STRING = "TestString";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Runnable runnable = new Runnable() {
            public void run() {
                SomeObject someObject = ObjectPoll.getInstance().pull(); // pull object by stack
                someObject.setSomeInt(TEST_NUMBER);
                someObject.setSomeString(TEST_STRING);
                someObject = null; //release and reset object
            }
        };

        //To create/receive recycled objects without doing anything
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        /*runnable ,l = initialDelay, l1- period, timeUnit*/
        executor.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.MILLISECONDS);
    }
}