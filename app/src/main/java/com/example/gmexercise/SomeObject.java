package com.example.gmexercise;

/**
 * Created by Noy Davidyan on 26/11/2021 , 07:37
 */

public class SomeObject{

    private int someInt;
    private String someString;
    CallBack listener;

    public void setListener(CallBack listener){
        this.listener = listener;
    }

    public SomeObject() {}

    public void setSomeInt(int someInt) {
        this.someInt = someInt;
    }
    public void setSomeString(String someString) {
        this.someString = someString;
    }

    //catches the object before killing by the GB and returns it to the stack for reusable
    protected void finalize(){

        if(listener != null)
            listener.returnItemToReuse(this);
    }

}
