package com.example.gmexercise;

import android.util.Log;

import java.util.Stack;

/**
 * Created by Noy Davidyan on 26/11/2021 , 07:16.
 */

public class ObjectPoll {

    private Stack<SomeObject> objectsPoolStack = new Stack<>();
    private static ObjectPoll instance;

    final String TAG = "ObjectPollTAG";// Just for test


    public static ObjectPoll getInstance() {
        if (instance == null)
            instance = new ObjectPoll();

        return instance;
    }

    public SomeObject pull() {

        SomeObject someObject;

        if (objectsPoolStack.isEmpty()) {
            someObject = new SomeObject();
            Log.d(TAG,"new object"); // Just for test
        } else {
            someObject = objectsPoolStack.pop();
            Log.d(TAG,"poll object");// Just for test
        }

         someObject.setListener(new CallBack() {
             @Override
             public void returnItemToReuse(SomeObject someObject) {
                 objectsPoolStack.push(someObject);
                 Log.d(TAG,"recycle object");// Just for test
             }
         });

        return someObject;
    }
}
